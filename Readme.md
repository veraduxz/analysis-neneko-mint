# Hi and welcome to this feature presentation of analysing Mint_Miracle and Neneko Wonderland
This document was apparently needed because instead of people trying to find out the truth - they decide to blindly believe someone. After evidence was shown refuting their statements, the actions undertaken were reversed. However, due to people clamoring onto drama because.. drama, I wanted to set the record straight. In this repo, you'll find both .ini files that were analysed, Mint_Miracle.ini being the Mint developed one on Moogleshade and NenekoWonderland.ini being Neneko's work. (https://twitter.com/Xelyanne) 

## Let's begin

### Plugins:
For those uninitiated, Reshade works by having several Shader plugins turned on or off and configured. This is what is the basis of the argument, it was said that the base of the ini's was the same. We'll start off with the plugins that are shared on both.

#### Clarity.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Clarity.fx is the origin.
~~~
// Neneko
ClarityBlendMode=0
ClarityRadius=3
ClarityOffset=2.000000
ClarityDarkIntensity=0.400000
ClarityBlendIfDark=50
ClarityBlendIfLight=205
ClarityStrength=0.400000
ClarityViewBlendIfMask=0
ClarityLightIntensity=0.000000
ClarityViewMask=0
~~~
~~~
// Mint_Miracle
ClarityBlendMode=2
ClarityRadius=20
ClarityOffset=5.000000
ClarityDarkIntensity=0.400000
ClarityBlendIfDark=50
ClarityBlendIfLight=205
ClarityStrength=0.400000
ClarityViewBlendIfMask=0
ClarityLightIntensity=0.000000
ClarityViewMask=0
~~~
~~~
Discrepancies: 
ClarityBlendMode
ClarityRadius
ClarityOffset
~~~
An argument can be made that there are similarities, but let's get into the points:

##### ClarityBlendMode
These are literally different lighting methods. As described in the original file, ClarityBlendMode either sets the shader to softlight or hardlight.

In NenekoWonderland, this is set to Softlight
In Mint_Miracle, this is set to Hardlight

This makes a difference in light and the casting of shadows in a picture. This has an impact on the entire shader.

##### ClarityRadius
This is the largest discrepancy. 3.00 vs 20.00 is a very significant difference. Alongside the other changes, this makes it very clear that aside from default values being used in the rest of the shader (which can be found and are similarly described in the repo file above) the plugin settings aren't at all similar.

##### ClarityOffset
This is probably another monumentary thing alongside the lighting changes for this plugin considering this is set to it's max value of 5.00 in UI for Mint_Miracle where NenekoWonderland keeps this at it's default of 2.00. 

In my conclusion, NenekoWonderland opts for a much softer and subtle approach to using the Clarity.fx shader, where as Mint_Miracle uses hard light to create harsh contrasts using the plugin. Aside from defaults being used, I don't think it's fair to say the two are similar based on this.

#### SMAA.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/SMAA.fx is the origin
~~~
// NenekoWonderland
EdgeDetectionType=1
EdgeDetectionThreshold=0.100000
MaxSearchSteps=98
MaxSearchStepsDiagonal=16
CornerRounding=0
DebugOutput=0
~~~
~~~
// Mint_Miracle
EdgeDetectionType=2
EdgeDetectionThreshold=0.050000
MaxSearchSteps=112
MaxSearchStepsDiagonal=17
CornerRounding=100
DebugOutput=0
~~~
~~~
Discrepancies:
EdgeDetectionType
EdgeDetectionTreshold
MaxSearchSteps
MaxSearchStepsDiagonal
CornerRounding
~~~
##### EdgeDetectionType
Default setting is 1 as described in the origin. Neneko uses the default for this setting. Which calls SMAAColorEdgeDetectionPS. MinT_Miracle uses 2 for this setting, which calls SMAADepthEdgeDetectionPS. 

The one culls aliased lines generally in the depth buffer, leaving the UI alone. While Color will do much of the same but will probably also process UI And text. I'd say this is noticeable enough during play to notice a difference.

##### EdgeDetectionTreshold
Default is 0.05 - so Neneko has made some changes to this. It's less sensitive to edges. Not much to say here. 0.05 difference won't matter much I'd think.

##### MaxSearchSteps
This is the Radius where the SMAA shader will search for aliased edges (as described in the origin). The differences between these are negligible, the only difference would be is performance, not effect.

##### MaxSearchStepsDiagonal
1 Difference, same idea as above, negligible difference. More of the same as written above.

##### Corner Rounding 
This is a major difference, none at all to very much. This means that Neneko does no Corner Rounding on their SMAA where as Mint_Miracle has set the highest setting. This has some pretty significant impact. I removes further aliasing at the cost (of sometimes) text looking wonky. 

Again, this is looking like harshness vs subtlety, Neneko opting for subtlety and Mint_Miracle for harshness.

#### Magicbloom.fx 
https://github.com/crosire/reshade-shaders/blob/master/Shaders/MagicBloom.fx is the origin.
~~~
// Neneko
f2Adapt_Clip=0.000000,1.000000
fBloom_Intensity=0.302000
fBloom_Threshold=3.900000
fDirt_Intensity=0.938000
fExposure=0.513000
fAdapt_Sensitivity=0.994000
fAdapt_Speed=0.588000
iDebug=0
iAdapt_Precision=1076677888
~~~
~~~
// Mint_Miracle
f2Adapt_Clip=0.000000,1.000000
fBloom_Intensity=1.383000
fBloom_Threshold=2.000000
fDirt_Intensity=0.344000
fExposure=0.500000
fAdapt_Sensitivity=1.000000
fAdapt_Speed=0.100000
iDebug=0
iAdapt_Precision=2
~~~
~~~
Discrepancies:
fBloom_Intensity
fBloom_Treshold
fExposure (very slightly)
fAdapt_Sensitivity (very slightly)
fAdapt_Speed
iAdapt_Precision
~~~

I won't go over the fExposure and fAdapt_Sensitivity attributes, the difference between both aren't significant enough to warrant a full discrepancy.

##### fBloom_Intensity
The amount of bloom added to the image. This is significant amount higher on Mint_Miracle, indicating much more bloom vs the more subtle Neneko approach (sensing the pattern here..?)

##### fBloom_Treshold
Exponential formula calculating the amount of dark pixels used for bloom. To take the excerpt from the origin: 
"Essentially, it increases the contrast in bloom and blackens darker pixels."
So, it does. This treshold is much higher on Neneko, indicating a stronger contrast. 

##### fAdapt_Speed
How quickly the bloom adapts. This is set much higher on Neneko, indicating the effect of bloom is shown much faster. Mint_Miracle uses the default setting of this shader.

##### iAdapt_Precision
Holy smokes batman! 1076677888 seems like a big value but it's relatively normal. 2 however means that most of the image is calculated into the bloom adaptation, whereas Neneko focuses mostly on the center of the image. 

This seems a bit of an opposite position, Neneko seems to be much harsher on the bloom effect, but the actual appearance seems much more subtle.

#### FineSharp.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/FineSharp.fx is the origin.

~~~ // Neneko
cstr=0.874000
sstr=0.560000
pstr=1.272000
xstr=0.190000
lstr=1.490000
xrep=0.250000
~~~ 

~~~ 
// Mint_Miracle
sstr=2.000000
cstr=0.900000
xstr=0.190000
pstr=1.272000
xrep=0.250000
lstr=1.490000
~~~  

~~~ 
Discrepancies:
sstr
cstr (Slightly)
~~~ 

The other values are the default of the shader, in fact, Mint_Miracle only uses the defaults. This was simply toggled on in that version.

#### Colourfulness.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Colourfulness.fx is the origin

~~~ 
// Neneko 
colourfulness=0.500000
lim_luma=0.700000
enable_dither=0
col_noise=1
backbuffer_bits=8.000000
~~~ 


~~~ 
// Mint_Miracle
colourfulness=0.714000
lim_luma=0.391000
enable_dither=0
col_noise=1
backbuffer_bits=8.000000
~~~ 

~~~ Discrepancies:
colourfulness
lim_luma
~~~ 

The others are defaults.

##### colourfulness
The amount of saturation change. Mint_Miracle is more saturated than Neneko, colours will be significantly different.

##### lim_luma
the brightness of colours. Mint_Miracle opts for darker deeper colors (in combination with the previous setting) instead of softer colours like NenekoWonderland.

How this processes the colours is significantly changing the way each picture looks. I'd say based on this alone the concept of each shader is so significantly different it's not worth mentioning more. 

#### MultiLUT.fx
The big one that started the argument. To start off with: LUT stands for "Look Up Textures". It generally is used in color-grading and depending on the LUT used it changes the colours and brightness.

https://github.com/crosire/reshade-shaders/blob/master/Shaders/MultiLUT.fx is the origin of the shader.

~~~ 
// Neneko
fLUT_LutSelector=12
fLUT_AmountChroma=1.000000
fLUT_AmountLuma=1.000000
~~~ 

~~~ 
// Mint_Miracle
fLUT_LutSelector=6
fLUT_AmountChroma=1.200000
fLUT_AmountLuma=0.500000
~~~ 
~~~
Discrepancies:
fLUT_LutSelector
fLUT_AmountChroma
fLUT_AmountLuma
~~~
Every property is different. How they even though that based on the question the shaders were similar is beyond me, but let's delve deeper into what it does.

#### fLUT_LutSelector
These aren't even close to similar. The changes this option makes alone are grounds to dismiss the entire thing as stupid. The options are:

~~~ 
Neutral
Color1
Color2
Color3 (Blue oriented)
Color4 (Hollywood)
Color5
Color6 (What Neneko is using)
Color7
Color8
Cool light
Flat & Green
Red lift matte
Cross process (What Mint_Miracle is using)
Azure Red Dual Tone
Sepia
B&W mid contrast
B&W high contrast.
~~~ 

Honestly, this just makes me angry.

##### fLUT_AmountChroma
Intensity of the color/chroma change of the LUT. This is a little higher on Mint_Miracle but default on Neneko. 

##### fLUT_AmountLuma
Intensity of the luma (brightness) change of the LUT. This is a lot lower on Mint_Miracle, making it once again darker but deeper colors combined with the last option.

Honestly, rounding this one up, I don't even know what to say. This thing started over a question about this shader specifically and they're nothing alike. This shouldn't stand.

#### LUT.fx 
https://github.com/crosire/reshade-shaders/blob/master/Shaders/LUT.fx is the origin.

~~~ 
// Neneko
fLUT_AmountChroma=0.690000
fLUT_AmountLuma=0.240000
~~~ 

~~~ 
// Mint_Miracle
fLUT_AmountChroma=0.676000
fLUT_AmountLuma=0.343000
~~~ 

While looking similar, the Luma value increases the intensity of the light from the LUT. So it should be a bit brighter than Neneko in the instance of this shader while opting for a little less color.

It's different enough to be able to say that they're not similar. 

#### Levels.fx 
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Levels.fx is the origin

~~~ 
// Neneko
BlackPoint=15
WhitePoint=235
HighlightClipping=0
~~~ 

~~~ 
// Mint_Miracle
BlackPoint=24
WhitePoint=233
HighlightClipping=0
~~~ 

So again, this seems very minor but this is based on 0-256 pixel RGB values. If it's darker than the blackpoint, it will simply be black (RGB 0, 0, 0) while if it's higher than the whitepoint it will be white (RGB 255, 255, 255)

The same pattern of "Neneko is softer on colors, while Mint's is harsher" continues. Mint cuts off earlier on the black pixels and the white pixels. This can be quite a significant change.

#### Sepia.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Sepia.fx is the origin

~~~ 
// Neneko
Tint=0.550000,0.430000,0.420000
Strength=0.580000
~~~ 

~~~ 
// Mint_Miracle
Tint=0.906977,0.836885,0.831044
Strength=0.229000
~~~ 

Neneko uses the defaults, Mint has actually changed these parameters very heavily.

Not much to say about this.

#### qUINT_mxao.fx
https://github.com/martymcmodding/qUINT/blob/master/Shaders/qUINT_mxao.fx is the origin (Reshade 4 shader)

~~~ 
// Neneko
qMXAO_FADE_DEPTH_END=0.400000
qMXAO_GLOBAL_SAMPLE_QUALITY_PRESET=4
qMXAO_SAMPLE_RADIUS=2.500000
qMXAO_AMOUNT_FINE=1.000000
qMXAO_SAMPLE_NORMAL_BIAS=0.200000
qMXAO_SAMPLE_RADIUS_SECONDARY=0.200000
qMXAO_GLOBAL_RENDER_SCALE=1.000000
qMXAO_SSAO_AMOUNT=1.000000
qMXAO_BLEND_TYPE=0
qMXAO_AMOUNT_COARSE=1.000000
qMXAO_FADE_DEPTH_START=0.050000
qMXAO_DEBUG_VIEW_ENABLE=0
~~~ 

~~~ 
// Mint_Miracle
MXAO_FADE_DEPTH_END=0.400000
MXAO_GLOBAL_SAMPLE_QUALITY_PRESET=2
MXAO_DEBUG_VIEW_ENABLE=0
MXAO_SAMPLE_RADIUS=2.500000
MXAO_SAMPLE_NORMAL_BIAS=0.200000
MXAO_FADE_DEPTH_START=0.050000
MXAO_GLOBAL_RENDER_SCALE=1.000000
MXAO_SSAO_AMOUNT=1.000000
MXAO_BLEND_TYPE=0
~~~ 
~~~
Discrepancies:
qMXAO_GLOBAL_SAMPLE_QUALITY_PRESET
~~~
A couple of properties aren't even similar on both and most are using their default settings. So is Mint_Miracle. The only thing changed by Neneko is the quality preset being set to a higher value. 

#### FXAA.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/FXAA.fx is the origin

~~~ 
// Both Neneko and Mint_Miracle
Subpix=0.250000
EdgeThreshold=0.125000
EdgeThresholdMin=0.000000
~~~ 

Defaults for the shader, so not much to say.

#### Deband.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Deband.fx is the origin

~~~ 
// Neneko
Threshold=0.005000
Range=1.000000
Iterations=3
Grain=0.003000
~~~ 

~~~ 
// Mint_Miracle
Threshold=0.004000
Range=16.000000
Iterations=1
Grain=0.006000
~~~ 

Everything's different in Neneko from Default. Mint simply uses the default settings for this shader.

#### Technicolor2.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Technicolor2.fx is the origin

~~~ 
// Neneko
Saturation=1.000000
ColorStrength=0.200000,0.200000,0.200000
Brightness=1.000000
Strength=1.000000
~~~ 

~~~ 
// Mint_Miracle
Saturation=0.686000
ColorStrength=0.431373,0.435294,0.439216
Brightness=1.195000
Strength=0.619000
~~~ 

Again a case of Defaults vs actually adjusted. Neneko uses defaults, Mint_Miracle has changed every property.

#### Layer.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Layer.fx is the origin

~~~ 
// Neneko
Layer_Blend=0.329000
Layer_Select=0
Layer_Scale=1.001000
Layer_PosX=0.500000
Layer_PosY=0.500000
~~~ 

~~~ 
// Mint_Miracle 
Layer_Blend=0.400000
~~~ 

Only thing that is seemingly different is the Layer_Blend property. There's a minor variation on the Layer_Scale for Neneko, but Mint_Miracle doesn't seem to touch on this shader much.

#### Curves.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Curves.fx is the origin

~~~ 
// Neneko
Mode=0
Formula=4
Contrast=0.650000
~~~ 

~~~ 
// Mint_Miracle 
Mode=1
Formula=2
Contrast=0.337000
~~~ 

Default being used by Neneko, Mint has made adjustments. Moving on.

#### qUINT_dof.fx
https://github.com/martymcmodding/qUINT/blob/master/Shaders/qUINT_dof.fx is the origin (Reshade 4)

~~~ 
//Neneko
bADOF_AutofocusEnable=1
fADOF_ShapeAnamorphRatio=1.000000
fADOF_AutofocusCenter=0.500000,0.500000
fADOF_AutofocusSpeed=0.100000
fADOF_AutofocusRadius=0.600000
fADOF_ManualfocusDepth=0.001000
fADOF_NearBlurCurve=6.000000
fADOF_HyperFocus=0.100000
fADOF_FarBlurCurve=1.500000
fADOF_RenderResolutionMult=0.500000
fADOF_ShapeRadius=20.500000
fADOF_SmootheningAmount=4.000000
fADOF_BokehIntensity=0.300000
fADOF_ShapeCurvatureAmount=1.000000
iADOF_ShapeVertices=6
iADOF_ShapeQuality=5
fADOF_ShapeRotation=0.000000
fADOF_ShapeChromaAmount=-0.100000
iADOF_ShapeChromaMode=2
~~~ 

~~~ 
//Mint_Miracle
bADOF_AutofocusEnable=4294967295
fADOF_ShapeAnamorphRatio=1.000000
fADOF_AutofocusCenter=0.500000,0.500000
fADOF_AutofocusSpeed=1.000000
fADOF_AutofocusRadius=1.000000
fADOF_ManualfocusDepth=1.000000
fADOF_NearBlurCurve=6.000000
fADOF_HyperFocus=1.000000
fADOF_FarBlurCurve=1.949000
fADOF_RenderResolutionMult=1.000000
fADOF_ShapeRadius=40.000000
fADOF_SmootheningAmount=4.026000
fADOF_BokehIntensity=0.730000
fADOF_ShapeCurvatureAmount=1.000000
iADOF_ShapeVertices=6
iADOF_ShapeQuality=5
fADOF_ShapeRotation=0.000000
fADOF_ShapeChromaAmount=-0.020000
iADOF_ShapeChromaMode=2
~~~ 
~~~
Discrepancies:
bADOF_AutofocusEnable
fADOF_AutofocusSpeed
fADOF_AutofocusRadius
fADOF_ManualfocusDepth
fADOF_HyperFocus
fADOF_FarBlurCurve
fADOF_RenderResolutionMult
fADOF_ShapeRadius
fADOF_SmootheningAmount (Slightly)
fADOF_BokehIntensity
fADOF_ShapeChromaAmount
~~~
That is a lot of discrepancies and a lot of these are significantly different. Many of them are also defaults versus actual changes once again. I'm not going to describe each and every one of them, but it's safe to say that these are very different from each other.

#### FilmicAnamorphSharpen.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/FilmicAnamorphSharpen.fx is the origin

~~~ 
// Neneko
Strength=1.500000
Coefficient=0
Offset=1.000000
Clamp=0.500000
Contrast=128
Preview=0
~~~ 

~~~ 
// Mint_Miracle
Strength=1.000000
Offset=1
Coefficient=0
Clamp=1.000000
Contrast=128
Preview=0
~~~ 

Both strength values are low compared to the default, furthermore the clamp is the only value to be changed much between the two. Mint's seems fairly default. Neneko's seems more specialized.

#### Vibrance.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/Vibrance.fx is the origin

~~~ 
// Neneko
Vibrance=0.250000
VibranceRGBBalance=1.000000,1.000000,1.000000
~~~ 

~~~ 
// Mint_Miracle
Vibrance=0.314000
VibranceRGBBalance=1.000000,1.000000,1.000000
~~~ 

Mint's is more vibrant (and thus saturated), and that's all that needs to be said here.

#### FakeHDR.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/FakeHDR.fx is the origin

~~~ 
// Neneko 
HDRPower=1.300000
fradius1=0.793000
radius1=0.793000
fHDRPower=1.300000
radius2=0.870000
fradius2=0.870000
~~~ 

~~~ 
// Mint_Miracle 
HDRPower=1.219000
radius1=0.880000
radius2=0.870000
~~~ 

Not all the variables are even used. Radius2 defaults to 0.87. HDRPower defaults to 1.30. Neneko does some more changes than Mint does.

#### LumaSharpen.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/LumaSharpen.fx is the origin

~~~ 
// Neneko
sharp_strength=0.650000
pattern=1
sharp_clamp=0.035000
offset_bias=1.000000
show_sharpen=0
~~~ 

~~~ 
// Mint_Miracle
sharp_strength=0.298000
pattern=1
sharp_clamp=0.035000
offset_bias=1.000000
show_sharpen=0
~~~ 

Neneko uses defaults, Mint does some adjustments. Moving on.

#### FilmicPass.fx
https://github.com/crosire/reshade-shaders/blob/master/Shaders/FilmicPass.fx is the origin

~~~ 
// Neneko
Linearization=0.750000
Strength=0.500000
Contrast=0.900000
Fade=0.150000
Bleach=0.360000
Saturation=0.180000
GreenCurve=1.000000
RedCurve=1.000000
BlueCurve=1.000000
BaseCurve=1.500000
BaseGamma=1.000000
EffectGamma=0.500000
EffectGammaR=1.000000
EffectGammaG=1.000000
EffectGammaB=1.000000
LumCoeff=0.149020,0.666667,0.737255
~~~ 

~~~ 
// Mint_Miracle
Strength=0.850000
Linearization=0.500000
Fade=0.400000
Contrast=1.000000
Bleach=0.000000
Saturation=-0.150000
RedCurve=1.000000
GreenCurve=1.000000
BlueCurve=1.000000
BaseCurve=1.500000
BaseGamma=1.000000
EffectGamma=0.650000
EffectGammaR=1.000000
EffectGammaG=1.000000
EffectGammaB=1.000000
LumCoeff=0.212656,0.715158,0.072186
~~~ 

Mint uses a default, Neneko has adjusted quite a bunch of things. It's getting old now.

### Conclusion
That's the analysis of the entire thing. I did this because claiming a config is fine if you have a basis and they actually are pretty similar, but there wasn't one and there was public shaming nonetheless. The shameful part of this is the fact that this was accepted and not further investigated. So I hope this will be able to resolve some of that. 